// ==UserScript==
// @name        Tootify
// @namespace   https://badrihippo.github.io
// @version     1
// @description restore the "toot!" button to mastodon
// @author      Badri Sunderarajan
// @match       *://(fosstodon.org|scholar.social)/*
// ==/UserScript==

// By the way, you can add your own Mastodon domain to the "@match" field to make it work there too :)

// This is the actual function that changes the buttons
function tootify() {
  
  // "Publish" button on smaller devices (opens the compose box)
  document.querySelectorAll('a.button[href="/publish"]').forEach(l => { l.innerText = 'Toot' })
  
  // "Publish!" button that actually sends out the post
  document.querySelectorAll('.compose-form__publish-button-wrapper button').forEach(b => { b.innerText = 'Toot!' })
}

// This is the function that activates everything on page load
(function() {
  window.onload = function() {
    
    // Run the tootify once
    tootify()
    
    // Run it again every time the location changes
    let lastUrl = location.href

    new MutationObserver(() => {
        const url = location.href;
        if (url !== lastUrl) {
          lastUrl = url;
          tootify();
        }
    }).observe(document, {subtree: true, childList: true})

  }
})()
